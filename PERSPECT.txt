ClrDraw
AxesOff
1→I
1→A
1→B
⁻1→C
1→D
⁻1→E
⁻1→F
1→G
⁻1→H
Goto R
Lbl R
If A<10:Goto A
If A=10:Goto B
End
Lbl A
A+I→A
B+I→B
C-I→C
D+I→D
E-I→E
F-I→F
G+I→G
H-I→H
Goto C
End
Lbl B
Repeat A≤2
If A≤2:Goto R
A-I→A
B-I→B
C+I→C
D-I→D
E+I→E
F+I→F
G-I→G
H+I→H
ClrDraw
Line(A,B,10,10
Line(C,D,⁻10,10
Line(E,F,⁻10,⁻10
Line(G,H,10,⁻10
Line(A,B,C,D
Line(C,D,E,F
Line(E,F,G,H
Line(G,H,A,B
End
Lbl C
ClrDraw
Line(A,B,10,10
Line(C,D,⁻10,10
Line(E,F,⁻10,⁻10
Line(G,H,10,⁻10
Line(A,B,C,D
Line(C,D,E,F
Line(E,F,G,H
Line(G,H,A,B
Goto R
End